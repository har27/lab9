/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author Harpreet Singh
 */
class Employee {
    private String name;
    private double wage;
    private int hours;
    public Employee(String n, double w, int h) {
        name = n ;
        wage = w ;
        hours = h ;
    }
    public String getName() {
        return this.name;
    }
    public double getWage() {
        return this.wage;
    }
    public int getHours() {
        return this.hours;
    }
    public void setName(String n) {
        name = n ;
    }
    public void setWage(Double w) {
        wage = w ;
    }
    public void setHours(int h) {
        hours = h ;
    }
    public double calculatePay() {
        return hours*wage;
    }
    class Manager extends Employee {
        private double bonus;
        public Manager(double b) {
            super(n , w , h );
            bonus = b;
        }
        public double getBonus() {
            return this.bonus;
        }
        public void setBonus(Double b) {
            bonus = b;
        }
        @Override
        public double calculatePay() {
            return super.calculatePay() + bonus;
        }
    }
    public class PayrollStimulation {
        public static void main(String[] args) {
            Employee e1 = new Employee("ABC", 15.0, 8);
            Manager m1 = new Manager("XYZ", 30.0, 5);
            System.out.println("Paycheque of Employee $" + e1.calculatePay());
            System.out.println("Paycheque of Manager $" + m1.calculatePay());
        }
}
