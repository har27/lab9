
import java.lang.ProcessBuilder.Redirect.Type;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author Harpreet Singh
 */

public class PayrollSimulation {

   public static void main(String[] args) {

       EmployeeFactory employeeFactory = EmployeeFactory.getInstance();
       Employee employee = employeeFactory.getEmployee(Type.MANAGER);
       System.out.println(employee.describeEmployee());
       System.out.println(employee.calculatePay());
       Employee employee2 = employeeFactory.getEmployee(Type.EMPLOYEE);
       System.out.println(employee2.describeEmployee());
       System.out.println(employee2.calculatePay());

   }

}

